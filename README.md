# Comunia.io.swift

[![CI Status](https://img.shields.io/travis/arunzzrip@gmail.com/Comunia.io.swift.svg?style=flat)](https://travis-ci.org/arunzzrip@gmail.com/Comunia.io.swift)
[![Version](https://img.shields.io/cocoapods/v/Comunia.io.swift.svg?style=flat)](https://cocoapods.org/pods/Comunia.io.swift)
[![License](https://img.shields.io/cocoapods/l/Comunia.io.swift.svg?style=flat)](https://cocoapods.org/pods/Comunia.io.swift)
[![Platform](https://img.shields.io/cocoapods/p/Comunia.io.swift.svg?style=flat)](https://cocoapods.org/pods/Comunia.io.swift)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Comunia.io.swift is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'Comunia.io.swift'
```
## Requirements

- Swift 5.0
- iOS 14.0 or later


## Usage

Connecting to Comunia platform

You need to obtain a API key from comunia platform to start using this package. So first go to Comunia Website and register for account and create a new App where you can find your API Key.

###### To initialize the ComuniaClient you need below paramters.

- API_KEY : Your api key
- USER_ID : Your user id (any valid string without special characters)

```swift
import Comunia_io_swift
```

Implement ComuniaConnectionManagerDelegate in your viewController

```swift
let comuniaManager = ComuniaManager()
comuniaManager.delegate = self
comuniaManager.ComuniaInitConnection(userName: USER_ID, apiKey: API_KEY)
 
 func comuniaConnectionSuccess() {
    // connection initialization success
 }
    
 func comuniaConnectionError(comuniaError error: String) {
    // connection error 
 }
```

#### Outgoing Video call

Implement ComuniaVideoViewDelegate in your viewController

##### Call to another user and video view intergration

- REMOTE_USER : receiver user id

```swift
// Set UI View - ComuniaWebView
@IBOutlet weak var comuniaWebView: ComuniaWebView!

// Implement delegate
comuniaWebView.delegate = self

//Sending a request to another user for video call
func requestVideoCallForRemoteUser() {
    comuniaWebView.makeVideoCall(contact: REMOTE_USER)
}

func comuniaVideoViewError(videoViewError error: String) {
    //  error handle video call
}

func onVideoCallEnd() {
    // call end for receiver side
}
```

```swift
// Audio enable true or false
comuniaWebView.isAudioEnable(isAudio: Bool)

// Video enable true or false
comuniaWebView.isVideoEnable(isVideo: Bool)

// call reject for app side
  /**
   - parameters:
     - sender: user id/receiver user id
  */
comuniaWebView.rejectCall(sender: String)
```

#### Incoming Video call

Implement ComuniaIncomingCallDelegate in your viewController

```swift
let comuniaManager = ComuniaManager()

comuniaManager.delegateForCellAction = self
comuniaManager.initIncomingCallAction()

// onSignal event received
  /**
   - parameters:
     - sender: Sender id
     - data: json string
  */
func sendComuniaOnSignal(sender: String, data: Any) {
    // event received comunia side and you should move to video viewController
}

override func viewDidDisappear(_ animated: Bool) {
    // deint for incoming call event
    comuniaManager.deinitIncomingCallAction()
}
```

After moving to the video viewController,

```swift
// call onInComingCallAction function
  /**
   - parameters:
     - sender: Sender id
     - data: json string 
  */
comuniaWebView.onInComingCallAction(sender: String, data: Any)

// onIncomingCallAction event received
func onIncomingCallAction() {
    // you can present incoming call notify view, Add call Answer and call reject action
}

//  Accept call request - 
  /**
   - parameters:
     - sender: Sender id
  */
comuniaWebView.acceptCallRequest(sender: String)

//  call reject - 
  /**
   - parameters:
     - sender: Sender id
  */
comuniaWebView.rejectCall(sender: String)
```

#### Message/ File Upload/ File Download

```swift
// Implement delegate
comuniaManager.delegateMessage = self

// Sending a message another user
  /**
   - parameters:
     - senderName: Sender id
     - message: message string
  */
comuniaManager.sendMessage(senderName: String, message: String)

// received Messsage
func receivedOnMesssage(message: String) {
    // received Messsage delegate function
}

func comuniaMessageError(comuniaError error: String, message: Any) {
    //  error handle message
}

// File Uploading
  /**
   - parameters:
     - senderName: Sender id
     - data: Data array
     - imageUrl: URL string
  */
comuniaManager.uploadFile(senderName: String, data: [Data], imageUrl: String)

// File Upload Success
  /**
   - parameters:
     - lastUploadedFileName: last uploaded file name
     - fileId: file Id
     - contentType: content Type
     - status: status
  */
func comuniaFileUploadSuccess(lastUploadedFileName: String, fileId: String, contentType: String, status: Int) {
   // file uploaded success 
}

func comuniaFileUploadError(comuniaError error: String) {
    //  error handle file upload
}

// File Download
 /**
   - parameters:
     - fileId: file id
     - fileType: file content type
  */
comuniaManager.fileDownload(fileId: String, fileType: String)

// File download Success
 func comuniaFileDownloadSuccess(message: String) {
    // file download success 
 }
 
func comuniaFileDownloadError(comuniaError error: String) {
    // error handle file download
}
```

## Author

Comunia Team

## License

Comunia.io.swift is available under the MIT license. See the LICENSE file for more info.
