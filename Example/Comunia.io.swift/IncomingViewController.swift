//
//  IncomingViewController.swift
//  Evensel sdk client
//
//  Created by Aruna Udayanga on 2021-06-05.
//  Copyright © 2021 Chamara. All rights reserved.
//

import UIKit

protocol IncomingCallBackDelegate{
    func incomingViewCallBackAction(eventJsonData: String, selectionType:String)
}

class IncomingViewController: UIViewController {

    @IBOutlet weak var lblCallType: UILabel!
    @IBOutlet weak var lblUser: UILabel!
    @IBOutlet weak var btnCallAnswer: UIButton!
    @IBOutlet weak var btnCallEnd: UIButton!
    
    var eventJsonObject:String = ""
    var callSender: String = ""
    var delegate:IncomingCallBackDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnCallAnswer.layer.cornerRadius = btnCallAnswer.frame.height / 2
        btnCallEnd.layer.cornerRadius = btnCallEnd.frame.height / 2
        lblUser.text = callSender
    }
    
    @IBAction func callAnswerBtnClicked(_ sender: Any) {
        delegate?.incomingViewCallBackAction(eventJsonData: eventJsonObject, selectionType: "Answer")
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func callEndBtnClicked(_ sender: Any) {
        delegate?.incomingViewCallBackAction(eventJsonData: eventJsonObject, selectionType: "CallEnd")
        self.dismiss(animated: false, completion: nil)
    }

}
