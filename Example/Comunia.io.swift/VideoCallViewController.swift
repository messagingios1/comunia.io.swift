//
//  VideoCallViewController.swift
//  Comunia.io.swift_Example
//
//  Created by Aruna Udayanga on 2021-06-16.
//  Copyright © 2021 CocoaPods. All rights reserved.
//

import UIKit
import Comunia_io_swift

class VideoCallViewController: UIViewController {

    @IBOutlet weak var receiverTextField: UITextField!
    
    let comuniaManager = ComuniaManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        comuniaManager.delegateForCellAction = self
        comuniaManager.initIncomingCallAction()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        comuniaManager.deinitIncomingCallAction()
    }
    
    @IBAction func videoCallBtnTapped(_ sender: Any) {
//        let webView = BottomsheetController()
//        webView.remotePeerName = self.receiverTextField.text ?? ""
//        webView.modalPresentationStyle = .fullScreen
//        self.present(webView, animated: true, completion: nil)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let videoCallVC = storyboard.instantiateViewController(withIdentifier: "VideoCallSampleViewController") as! VideoCallSampleViewController
        videoCallVC.remotePeerName = self.receiverTextField.text ?? ""
        videoCallVC.modalPresentationStyle = .overFullScreen
        self.present(videoCallVC, animated: true, completion: nil)
        
    }
    
    @IBAction func messageBtnTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let videoCallVC = storyboard.instantiateViewController(withIdentifier: "MessageViewController") as! MessageViewController
        videoCallVC.senderName = self.receiverTextField.text ?? ""
        videoCallVC.modalPresentationStyle = .overFullScreen
        self.present(videoCallVC, animated: true, completion: nil)
        
    }
}

extension VideoCallViewController: ComuniaIncomingCallDelegate {
    
    func sendComuniaOnSignal(sender: String, data: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let videoCallVC = storyboard.instantiateViewController(withIdentifier: "VideoCallSampleViewController") as! VideoCallSampleViewController
        videoCallVC.sender = sender
        videoCallVC.jsonObject = data
        videoCallVC.incommingCallType = true
        videoCallVC.remotePeerName = self.receiverTextField.text ?? ""
        videoCallVC.modalPresentationStyle = .overFullScreen
        self.present(videoCallVC, animated: true, completion: nil)
    }
}
