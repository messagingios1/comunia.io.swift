//
//  VideoCallSampleViewController.swift
//  Comunia.io.swift_Example
//
//  Created by Aruna Udayanga on 2021-06-17.
//  Copyright © 2021 CocoaPods. All rights reserved.
//

import UIKit
import Comunia_io_swift

class VideoCallSampleViewController: UIViewController {

    @IBOutlet weak var comuniaWebView: ComuniaWebView!
    @IBOutlet weak var btnAudio: UIButton!
    @IBOutlet weak var btnVideo: UIButton!
    
    var remotePeerName: String = ""
    var sender: String = ""
    var jsonObject : Any!
    var incommingCallType: Bool = false
    var isIncomming = false
    
    var isAudio:Bool = true
    var isVideo:Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        comuniaWebView.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        inComingPopupView()
    }
    
    func inComingPopupView(){
        if incommingCallType {
            self.incommingCallType = false
            comuniaWebView.onInComingCallAction(sender: sender, data: jsonObject)
        }
    }
    
    // MARK: Button Action
    @IBAction func audioBtnTapped(_ sender: Any) {
        isAudio = !isAudio
        btnAudio.setImage(UIImage(named: isAudio == true ? "microphone" : "mute_microphone"), for: .normal)
        comuniaWebView.isAudioEnable(isAudio:isAudio)
    }
    
    @IBAction func videoBtnTapped(_ sender: Any) {
        isVideo = !isVideo
        btnVideo.setImage(UIImage(named: isAudio == true ? "video-camera" : "video-camera"), for: .normal)
        comuniaWebView.isVideoEnable(isVideo:isVideo)
    }
    
    @IBAction func callEndBtnClicked(_ sender: Any) {
        if isIncomming {
            comuniaWebView.rejectCall(sender: self.sender)
        } else {
            comuniaWebView.rejectCall(sender: self.remotePeerName)
        }
        self.dismiss(animated: true, completion: nil)
    }
}

extension VideoCallSampleViewController: ComuniaVideoViewDelegate {
    
    func comuniaVideoViewError(videoViewError error: String) {
        let alert = UIAlertController(title: "ERROR", message: error, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func requestVideoCallForRemoteUser() {
        comuniaWebView.makeVideoCall(contact: remotePeerName)
    }
    
    func onIncomingCallAction() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let incomingVC = storyboard.instantiateViewController(withIdentifier: "IncomingViewController") as! IncomingViewController
        incomingVC.delegate = self
        incomingVC.modalPresentationStyle = .overFullScreen
        self.present(incomingVC, animated: false, completion: nil)
    }
    
    func onVideoCallEnd() {
        self.dismiss(animated: false, completion: nil)
    }
}

extension VideoCallSampleViewController: IncomingCallBackDelegate {
    
    func incomingViewCallBackAction(eventJsonData: String, selectionType: String) {
        self.isIncomming = true
        if selectionType == "Answer" {
            comuniaWebView.acceptCallRequest(sender: self.sender)
        } else {
            comuniaWebView.rejectCall(sender: self.sender)
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}
