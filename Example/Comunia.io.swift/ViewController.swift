//
//  ViewController.swift
//  Comunia.io.swift
//
//  Created by arunzzrip@gmail.com on 06/15/2021.
//  Copyright (c) 2021 arunzzrip@gmail.com. All rights reserved.
//

import UIKit
import Comunia_io_swift

class ViewController: UIViewController {
    
    var sApiKey = "ae9823f50ba8eb46f4f5b7aadba39079b4228a6cbda4c34132c68815e638cecd4b0a7263cf18d44240f71fd28bec2f89eccaa233018550934014d854ab4c4bde008b715a52073e6885a10090ce4b856194b7c7e06b948950bc799e4629882e6a6a51b8d83be40b7cfa78eec7933396f2f8177fe22fde090c852560eca71754a8"
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var apiKeyTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        apiKeyTextField.text = sApiKey
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func createConnectionBtnTapped(_ sender: Any) {
    
        let comuniaManager = ComuniaManager()
        comuniaManager.delegate = self
        comuniaManager.ComuniaInitConnection(userName: usernameTextField.text!, apiKey: sApiKey)
    }
}

extension ViewController: ComuniaConnectionManagerDelegate {
    
    func comuniaConnectionSuccess() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let videoCallVC = storyboard.instantiateViewController(withIdentifier: "VideoCallViewController") as! VideoCallViewController
        videoCallVC.modalPresentationStyle = .overFullScreen
        self.present(videoCallVC, animated: true, completion: nil)
    }
    
    func comuniaConnectionError(comuniaError error: String) {
        print("Connection Error => \(error)")
    }
}

