//
//  MessageViewController.swift
//  Comunia.io.swift_Example
//
//  Created by Aruna Udayanga on 2021-07-10.
//  Copyright © 2021 CocoaPods. All rights reserved.
//

import UIKit
import MobileCoreServices
import UniformTypeIdentifiers
import Comunia_io_swift
import Photos

class MessageViewController: UIViewController {
    
    @IBOutlet weak var sendMessage: UITextField!
    @IBOutlet weak var messageTableView: UITableView!
    var user: String = ""
    var senderName:String = ""
    var messageList: Array<ChatMessage> = []
    var lastUploadedFileName:String = ""
    var lastUploadedFileId:String = ""
    var lastUploadedFileType:String = ""
    
    
    let comuniaManager = ComuniaMessage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        messageTableView.rowHeight = UITableView.automaticDimension
        messageTableView.estimatedRowHeight = 300
       
        comuniaManager.delegateMessage = self
    }
    
    @IBAction func sendBtnClicked(_ sender: UIButton) {
        let messageItem = ChatMessage(s_Id: "", l_Time: currentTime(), s_From: user, s_To: senderName, s_Data: sendMessage.text!)
        
        messageList.append(messageItem)
        messageTableView.reloadData()
        sendChatMessage()
    }
    
    private func sendChatMessage(){
        comuniaManager.sendMessage(senderName: senderName, message: sendMessage.text ?? "")
        sendMessage.text = ""
    }
    
    @IBAction func fileDownloadClicked(_ sender: Any) {
        
        if(lastUploadedFileId.count == 0){
            return
        }
        
        comuniaManager.fileDownload(fileId: lastUploadedFileId, fileType: lastUploadedFileType)
    }
    
    
    @IBAction func fileShareBtnClicked(_ sender: Any) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        // Cancel
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (res) in
            
        }
        // Camera
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (res) in
            self.showImagePicker(sourceType: UIImagePickerController.SourceType.camera)
        }
        // Photo & Video Library
        let photoAndVideoAction = UIAlertAction(title: "Photo & Video Library", style: .default) { (res) in
            self.showImagePicker(sourceType: UIImagePickerController.SourceType.photoLibrary)
        }
        // Document
        let documentAction = UIAlertAction(title: "Document", style: .default) { (res) in
            self.showDocumentPicker()
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(cameraAction)
        alertController.addAction(photoAndVideoAction)
        alertController.addAction(documentAction)
        
        
        self.present(alertController, animated: true) {
            
        }
    }
    
    func showDocumentPicker(){
        let documentPickerController = UIDocumentPickerViewController(documentTypes:
                                                                        [
                                                                            String(kUTTypeAudiovisualContent),
                                                                            String(kUTTypePDF),
                                                                            String(kUTTypeImage),
                                                                            String(kUTTypeMovie),
                                                                            String(kUTTypeVideo),
                                                                            String(kUTTypePlainText),
                                                                            String(kUTTypeMP3),
                                                                            String(kUTTypePNG)
                                                                        ], in: .import)
        documentPickerController.delegate = self
        present(documentPickerController, animated: true, completion: nil)
    }
    
    func showImagePicker(sourceType: UIImagePickerController.SourceType){
        if UIImagePickerController.isSourceTypeAvailable(sourceType){
            
            let imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = sourceType;
            imag.mediaTypes = [String(kUTTypeImage)]
            imag.allowsEditing = false
            self.present(imag, animated: true, completion: nil)
        }
    }
    
    private func currentTime() -> Int{
        let someDate = Date()
        // convert Date to TimeInterval (typealias for Double)
        let timeInterval = someDate.timeIntervalSince1970
        // convert to Integer
        let myInt = Int(timeInterval)
        return myInt
    }
    
    @IBAction func btnBackTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension MessageViewController: UITableViewDataSource  {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let messageItem = messageList[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageTableViewCell", for: indexPath) as! MessageTableViewCell
        
        if(messageItem.s_Id == ""){
            cell.reciveMessage.text = ""
            cell.sendmessage.text = messageItem.s_Data
            cell.reciveView.isHidden = true
            cell.sendView.isHidden = false
        }
        else{
            cell.reciveMessage.text = messageItem.s_Data
            cell.sendmessage.text = ""
            cell.reciveView.isHidden = false
            cell.sendView.isHidden = true
        }
        
        return cell
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageList.count
    }
    
}

// MARK: - Table View Delegate
extension MessageViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension MessageViewController: ComuniaMessageDelegate {
    
    func receivedOnMesssage(message: String) {
        let jsonData = message.data(using: .utf8)!
        let decoder = JSONDecoder()
        let messageObject = try! decoder.decode(ChatMessage.self, from: jsonData)
        messageList.append(messageObject)
        messageTableView.reloadData()
    }
    
    func comuniaMessageError(comuniaError error: String, message: Any) {
        
    }
    
    func comuniaFileUploadSuccess(lastUploadedFileName: String, fileId: String, contentType: String, status: Int) {
        
        DispatchQueue.main.async {
            let messageItem = ChatMessage(s_Id: "", l_Time: self.currentTime(), s_From: self.user, s_To: self.senderName, s_Data: lastUploadedFileName)
            
            self.messageList.append(messageItem)
            self.messageTableView.reloadData()
            self.lastUploadedFileId = fileId
            self.lastUploadedFileType = contentType
        }
    }
    
    func comuniaFileUploadError(comuniaError error: String) {
        
    }
    
    func comuniaFileDownloadSuccess(message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Success", message: "File Downloaded to \(message)", preferredStyle: .alert)
            alert.addAction(
                UIAlertAction(title: "OK", style: .default, handler: { action in
                 
                }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func comuniaFileDownloadError(comuniaError error: String) {
        
    }
}

extension MessageViewController: UIImagePickerControllerDelegate{

     func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

        let image = info[.originalImage] as? UIImage
        let data = image?.jpegData(compressionQuality: 1)
        let imageURL = info[.imageURL] as! URL
        comuniaManager.uploadFile(senderName: self.senderName, data: [data!], imageUrl:imageURL.absoluteString)
        self.dismiss(animated: true, completion: nil)

    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}


extension MessageViewController: UIDocumentPickerDelegate,UINavigationControllerDelegate{
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        print("url = \(urls)")
        guard let url = urls.first else { return }
        print("TZ \(url)")
        
        do {
            let data = try Data(contentsOf: url)
            comuniaManager.uploadFile(senderName: self.senderName, data: [data], imageUrl: url.absoluteString)
        } catch let err {
            print("TZ \(err)")
        }
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        self.dismiss(animated: true, completion: nil)
    }
}


public struct ChatMessage: Codable {
    var s_Id:String = ""
    var l_Time: Int = 0
    var s_From: String = ""
    var s_To: String = ""
    var s_Data: String = ""
}

