//
//  MessageTableViewCell.swift
//  Evensel sdk client
//
//  Created by Chamara on 9/19/20.
//  Copyright © 2020 Chamara. All rights reserved.
//

import UIKit

class MessageTableViewCell: UITableViewCell {
    @IBOutlet weak var reciveMessage: UILabel!
    @IBOutlet weak var sendmessage: UILabel!
    @IBOutlet weak var reciveView: UIView!
    @IBOutlet weak var sendView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        setUI()
    }

    func setUI(){
        reciveView.layer.cornerRadius = reciveView.frame.height/2
        sendView.layer.cornerRadius = sendView.frame.height/2
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
