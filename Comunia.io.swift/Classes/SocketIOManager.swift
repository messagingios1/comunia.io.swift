//
//  SocketIOManager.swift
//  Comunia.io.swift
//
//  Created by Aruna Udayanga on 2021-06-16.
//

import Foundation
import SocketIO
import SwiftyJSON

public protocol Callback{
    func onRegister(pSkt: SocketIOManager, lId: String)
    func onRegisterErr(pSkt: SocketIOManager, lId: String, sErr: String)
    func onLogin(pSkt: SocketIOManager, sGrp: String, sType: String, sName: String)
    func onRelogin(pSkt: SocketIOManager)
    func onLoginErr(pSkt: SocketIOManager, sErr: String)
    func onMsg(pSkt: SocketIOManager, lId: UInt64, sFrom: String, sMsg: String)
    func onMessage(message: Message)
    func onSig(pSkt: SocketIOManager, lId: UInt64, sFrom: String, sMsg: String)
    func onReq(pSkt: SocketIOManager, lId: UInt64, sFrom: String, sReq: String)
    func onSendReqRet(pSkt: SocketIOManager, lId: UInt64, sFrom: String, sRet: String)
    func onRes(pSkt: SocketIOManager, lId: UInt64, sFrom: String, sRes: String)
    func onCmd(pSkt: SocketIOManager, lId: UInt64, sFrom: String, sCmd: String)
    func onCmdRes(pSkt: SocketIOManager, lId: UInt64, sFrom: String, sCmd: String)
    func onGet(pSkt: SocketIOManager, lId: UInt64, sFrom: String, sGet: String)
    func onGetRes(pSkt: SocketIOManager, lId: UInt64, sFrom: String, sRes: String)
    func onHttp(pSkt: SocketIOManager, lId: UInt64, pData: JSON)
    func onPingRet(pSkt: SocketIOManager, lId: UInt64, pData: JSON)
    func onCfgStartRet(pSkt: SocketIOManager, lId: UInt64, sRet: String)
    func onCfgClsRet(pSkt: SocketIOManager, lId: UInt64, sRet: String)
    func onFileStart(pSkt: SocketIOManager, lId: UInt64, sFrom: String, iLen: Int, iPktSize: Int, sMsg: String)
    func onFileProgress(pSkt: SocketIOManager, lId: UInt64, sFrom: String, iProgress: Int, sMsg: String)
    func onFile(pSkt: SocketIOManager, lId: UInt64, sFrom: String, pFile: FileManager, sMsg: String)
    func onErr(pSkt: SocketIOManager, sErr: String)
    func isLowBandwidth(pSkt: SocketIOManager) -> Bool
    func onUploadFileSuccess(message:Any)
    func onUploadFileErr()
    func onDownloadFileSuccess(message:Any)
    func onDownloadFileErr()
    func onSignalResponse(sender: String, data: Any)
    func onIceConfigsResponse(call_id: String, JSONObject: Any)
}

public class SocketIOManager{
    var p_socket: SocketIOClient
    var e_State: State
    var s_Usr: String
    var s_Pwd: String
    var p_JDeviceInfo: [String:String]
    var s_SesId: String = ""
    var p_Callback: Callback? = nil
    var b_Logged: Bool = false
    var b_Disconnected: Bool = false
    static var l_LoggedTime: UInt64 = 0
    var manager: SocketManager!
    var fileService:FileService

    
    public init(sUrl: String, sUsr: String, sPwd: String, pDeviceInfo: [String:String]){
        self.s_Usr = sUsr
        self.e_State = .Init
        self.s_Pwd = sPwd
        self.p_JDeviceInfo = pDeviceInfo
        self.fileService = FileService(url:sUrl,token: "")
        
        manager = SocketManager(socketURL: URL(string: sUrl)!, config: [.log(true), .forcePolling(true)])
        self.p_socket = manager.defaultSocket
        
        socketOn()
        
        p_socket.on(clientEvent: .connect) { (data, ack) in
            print("on_connect(\(data))")
            self.onConnect()
        }
        
        p_socket.on(clientEvent: .disconnect) { (data, ack) in
            print("on_disconnect(\(data))")
            self.onDisconnect()
        }
        
        p_socket.on(clientEvent: .error) { (data, ack) in
            print("on_error(\(data))")
            self.onErr(sErr: "exception [\(data[0])]")
            self.disconnect()
        }
        
        p_socket.connect()
    }
    
    func socketOn(){
        p_socket.on("usr_add_ret") { (data, ack) in
            print("usr_add_ret(\(data))")
            
            let pRet = data[0] as! [String: Any]
            
            print("usr_add_ret = \(pRet)")
            
            if pRet["message"] as! String == "success"{
                self.onRegister(lId: pRet["id"] as! String)
            }
            else{
                self.onRegisterErr(lId: pRet["id"] as! String, sErr: pRet["msg"] as! String)
            }
        }
        
        p_socket.on("login_ret") { (data, ack) in
            print("usr_add_ret(\(data))")
            
            let pRet = data[0] as! [String: Any]
            
            print("usr_add_ret = \(pRet)")
            
            if pRet["message"] as! String == "success"{
                self.onRegister(lId: pRet["id"] as! String)
            }
            else{
                self.onRegisterErr(lId: pRet["id"] as! String, sErr: pRet["msg"] as! String)
            }
        }
        
        p_socket.on("login_response") { [self] (data, ack) in
            print("usr_add_ret(\(data))")
            
            let pRet = data[0] as! [String: Any]
            
            print("usr_add_ret = \(pRet)")
            
            if pRet["message"] as! String == "success"{
                self.fileService.token = pRet["jwtAccessToken"] as! String
                self.onLogin(sGrp: "grp", sName: "type", sType: "name")
            }
            else{
                self.onLoginErr(sErr: "login error [\(pRet["message"] as! String)]")
            }
        }
        
        p_socket.on("sig") { (data, ack) in
            print("on_sig(\(data))")
            
            self.onSig(pMsg: data[0] as! JSON)
        }
        
        p_socket.on("cfg_start_ret") { (data, ack) in
            print("on_cfgStartRet(\(data)")
            
            self.onCfgStartRet(pRet: data[0] as! JSON)
        }
        
        p_socket.on("cfg_cls_ret") { (data, ack) in
            print("on_cfg_cls_ret(\(data))")
            
            self.onCfgClsRet(pRet: data[0] as! JSON)
        }
        
        p_socket.on("req") { (data, ack) in
            print("on_req(\(data))")
            
            self.onReq(pReq: data[0] as! JSON)
        }
        
        p_socket.on("send_msg_ret") { (data, ack) in
            print("on_req(\(data))")
            
            self.onReq(pReq: data[0] as! JSON)
        }
        
        p_socket.on("send_req_ret") { (data, ack) in
            print("on_send_req_ret(\(data))")
            
            self.onSendReqRet(pReq: data[0] as! JSON)
        }
        
        p_socket.on("res") { (data, ack) in
            print("on_res(\(data))")
            
            self.onRes(pRes: data[0] as! JSON)
        }
        
        p_socket.on("cmd") { (data, ack) in
            print("on_cmd(\(data))")
            
            self.onCmd(pCmd: data[0] as! JSON)
        }
        
        p_socket.on("cmd_res") { (data, ack) in
            print("on_cmd_res(\(data))")
            
            self.onCmdRes(pRet: data[0] as! JSON)
        }
        
        p_socket.on("get") { (data, ack) in
            print("on_get(\(data))")
            
            self.onGet(pGet: data[0] as! JSON)
        }
        
        p_socket.on("get_res") { (data, emitter) in
            print("on_get_res(\(data))")
            
            self.onGetRes(pRes: data[0] as! JSON)
        }
        
        p_socket.on("http") { (data, ack) in
            self.onHttp(pData: data[0] as! JSON)
        }
        
        p_socket.on("ping_ret") { (data, ack) in
            self.onPingRet(pJPingRet: data[0] as! JSON)
        }
        
        p_socket.on("signal") { (data, ack) in
            print("onSignal(\(data))")
            let pRet = data[0] as! [String: Any]
            
            self.onSignalResponse(sender: pRet["sender"] as! String, data: pRet["data"] as Any)
        }
        
        p_socket.on("ice_configs_response") { (data, ack) in
            print("ice_configs_response : (\(data))")
            let pRet = data[0] as! [String: Any]
            
            self.onIceConfigsResponse(callId: pRet["call_id"] as! String, dataObj: pRet["data"] as Any)
        }
        
        p_socket.on("message") { (data, ack) in
            print("on_msg(\(data))")
            
            let pRet = data[0] as! [String: Any]
            
            print("on_msg message = \(pRet)")
            
            if pRet["type"] as! String == "new_message"{
                print("message \(pRet["data"] as! String)")
                
                if let d = pRet["files"] as? Data {
                    
                    do {
                        let uploadFileResponse = try JSONDecoder().decode(UploadFileResponse.self, from: d)
                        print("on_msg message = \(uploadFileResponse)")
                        
                        let message = Message(s_Id: pRet["message_id"] as! String, l_Time: pRet["message_received_time"] as! Int, s_From: pRet["sender"] as! String, s_To: "", s_Data: (pRet["data"] as! String).fromBase64()!)
                        
                        self.onMessage(message: message)
                        
                    } catch let decodeError {
                        debugPrint("TZ \(decodeError)")
                    }
                    
                }else{
                    let message = Message(s_Id: pRet["message_id"] as! String, l_Time: pRet["message_received_time"] as! Int, s_From: pRet["sender"] as! String, s_To: "", s_Data: (pRet["data"] as! String).fromBase64()!)
                    
                    self.onMessage(message: message)
                }
            }
        }
        
        p_socket.on("message_received") { (data, ack) in
            print("on_msg(\(data))")
            
            let pRet = data[0] as! [String: Any]
            
            print("on_msg = \(pRet)")
            
            if pRet["status"] as! Int == 200{
                print("message id \(pRet["message_id"] as! String)")
                let message = Message(s_Id: pRet["message_id"] as! String,
                                      l_Time: 0,
                                      s_From: "",
                                      s_To: "",
                                      s_Data: "")
                
                self.onMessage(message: message)
            } else{
                
            }
        }
    }
    
    func onConnect(){
        e_State = .Connected
        login()
    }
    
    func onDisconnect(){
        e_State = .Init
    }
    
    func onErr(sErr: String){
        p_Callback?.onErr(pSkt: self, sErr: sErr)
    }
    
    func onRegister(lId: String){
        p_Callback?.onRegister(pSkt: self, lId: lId)
    }
    
    func onRegisterErr(lId: String, sErr: String){
        p_Callback?.onRegisterErr(pSkt: self, lId: lId, sErr: sErr)
    }
    
    func onLogin(sGrp: String, sType: String, sName: String){
        SocketIOManager.l_LoggedTime = UInt64(Date().timeIntervalSince1970 * 1000)
        
        self.e_State = .Logged
        
        if(!self.b_Logged){
            p_Callback?.onLogin(pSkt: self, sGrp: sGrp, sType: sType, sName: sName)
        }
        else{
            p_Callback?.onRelogin(pSkt: self)
        }
        
        b_Logged = true
    }
    
    func onMsg(pMsg: JSON){
        let lId = pMsg["i_Id"].uInt64!
        let sFrom = pMsg["s_From"].string!
        let sData = pMsg["p_Msg"].string!
        
        p_Callback?.onMsg(pSkt: self, lId: lId, sFrom: sFrom, sMsg: sData)
    }
    
    func onSig(pMsg: JSON){
        let lId = pMsg["i_Id"].uInt64!
        let sFrom = pMsg["s_From"].string!
        let sData = pMsg["p_Msg"].string!
        
        p_Callback?.onSig(pSkt: self, lId: lId, sFrom: sFrom, sMsg: sData)
    }
    
    func onCfgStartRet(pRet: JSON){
        let lId = pRet["i_Id"].uInt64!
        let sData = pRet["p_Msg"].string!
        
        p_Callback?.onCfgStartRet(pSkt: self, lId: lId, sRet: sData)
    }
    
    func onCfgClsRet(pRet: JSON){
        let lId = pRet["i_Id"].uInt64!
        let sData = pRet["p_Msg"].string!
        
        p_Callback?.onCfgClsRet(pSkt: self, lId: lId, sRet: sData)
    }
    
    func onReq(pReq: JSON){
        let lId = pReq["i_Id"].uInt64!
        let sFrom = pReq["s_From"].string!
        let sData = pReq["p_Msg"].string!
        
        p_Callback?.onReq(pSkt: self, lId: lId, sFrom: sFrom, sReq: sData)
    }
    
    func onSendReqRet(pReq: JSON){
        let lId: UInt64 = 0;
        let sFrom = pReq["s_From"].string!
        let sData = pReq["p_Msg"].string!
        
        p_Callback?.onSendReqRet(pSkt: self, lId: lId, sFrom: sFrom, sRet: sData)
    }
    
    func onRes(pRes: JSON){
        let lId = pRes["i_Id"].uInt64!
        let sFrom = pRes["s_From"].string!
        let sData = pRes["p_Msg"].string!
        
        p_Callback?.onRes(pSkt: self, lId: lId, sFrom: sFrom, sRes: sData)
    }
    
    func onCmd(pCmd: JSON){
        let lId = pCmd["i_Id"].uInt64!
        let sFrom = pCmd["s_From"].string!
        let sData = pCmd["p_Msg"].string!
        
        p_Callback?.onCmd(pSkt: self, lId: lId, sFrom: sFrom, sCmd: sData)
    }
    
    func onCmdRes(pRet: JSON){
        let lId = pRet["i_Id"].uInt64!
        let sFrom = pRet["s_From"].string!
        let sData = pRet["p_Msg"].string!
        
        p_Callback?.onCmdRes(pSkt: self, lId: lId, sFrom: sFrom, sCmd: sData)
    }
    
    func onGet(pGet: JSON){
        let lId = pGet["i_Id"].uInt64!
        let sFrom = pGet["s_From"].string!
        let sData = pGet["p_Msg"].string!
        
        p_Callback?.onGet(pSkt: self, lId: lId, sFrom: sFrom, sGet: sData)
    }
    
    func onGetRes(pRes: JSON){
        let lId = pRes["i_Id"].uInt64!
        let sFrom = pRes["s_From"].string!
        let sData = pRes["p_Msg"].string!
        
        p_Callback?.onGetRes(pSkt: self, lId: lId, sFrom: sFrom, sRes: sData)
    }
    
    func onHttp(pData: JSON){
        let lId = pData["i_Id"].uInt64!
        let sData = pData["p_Msg"]
        
        p_Callback?.onHttp(pSkt: self, lId: lId, pData: sData)
    }
    
    func onPingRet(pJPingRet: JSON){
        let lId = pJPingRet["i_Id"].uInt64!
        let sData = pJPingRet["p_Msg"]
        
        p_Callback?.onPingRet(pSkt: self, lId: lId, pData: sData)
    }
    
    func onSignalResponse(sender: String, data: Any){
        print(sender)
        print(data)
        p_Callback?.onSignalResponse(sender: sender, data: data)
    }
    
    func onIceConfigsResponse(callId: String, dataObj: Any){
        print(callId)
        print(dataObj)
        p_Callback?.onIceConfigsResponse(call_id: callId, JSONObject: dataObj)
    }
    
    func login(){
        let login = ["user_id": s_Usr, "api_key": s_Pwd, "device_info": p_JDeviceInfo] as [String : Any]
        p_socket.emit("login", login)
    }
    
    func onLogin(sGrp: String, sName: String, sType: String){
        e_State = .Logged
        if(!b_Logged){
            p_Callback?.onLogin(pSkt: self, sGrp: sGrp, sType: sType, sName: sName)
        }
        else{
            p_Callback?.onRelogin(pSkt: self)
        }
        
        b_Logged = true
    }
    
    func onLoginErr(sErr: String){
        p_Callback?.onLoginErr(pSkt: self, sErr: sErr)
    }
    
    func onMessage(message: Message){
        p_Callback?.onMessage(message: message)
    }
    
    public func sendMsg(sId: String, sTo: String, sMsg: String, type: String){
        sendMsg(sId: sId, sTo: sTo, sMsg: sMsg, type: type, iExpireTime: 1000)
    }
    
    
    func sendMsg(sId: String, sTo: String, sMsg: String, type: String, iExpireTime: Int){
        let send_message = ["message_id": sId, "receiver": sTo, "data": sMsg.toBase64(), "type": type, "expire_time": iExpireTime] as [String : Any]
        
        p_socket.emit("send_message", send_message)
    }
    
    public func sendMsgWithFile(sId: String, sTo: String, sMsg: String, type: String, iExpireTime: Int, fileInfo:Any){
        
        do {
            let fileInfoRes = fileInfo as! [UploadFileResponse]
            let jsonData = try JSONEncoder().encode(fileInfoRes[0])
            
            let send_message = ["message_id": sId,
                                "receiver": sTo,
                                "data": sMsg.toBase64(),
                                "type": type,
                                "expire_time": iExpireTime,
                                "files": jsonData] as [String : Any]
            
            p_socket.emit("send_message", send_message)
            
        } catch let err {
            print(err)
        }
    }
    
    public func uploadFiles(to:String,files:[Data]){
        
        var fileInfoArray:[UploadFileResponse] = []
        
        let group = DispatchGroup()
        for f in files {
            group.enter()
            let data = f
            self.fileService.uploadFiles(data: data) { [self] (res) in
                print(res)
                
                guard let fileInfo = res as? UploadFileResponse else {
                    p_Callback?.onUploadFileErr()
                    return
                }
                
                fileInfoArray.append(fileInfo)
                group.leave()
            }
        }
        
        group.wait()
        p_Callback?.onUploadFileSuccess(message: fileInfoArray)
    }
    
    public func downloadFile(fileId:String,fileType:String){
        self.fileService.downloadFile(fileId: fileId,fileType:fileType) {[self] (res) in
            print("TZ \(res)")
            
            if let fileURL = res as? URL {
                p_Callback?.onDownloadFileSuccess(message: fileURL)
            }else{
                p_Callback?.onDownloadFileErr()
            }
        }
    }
    
    func disconnect(){
        print("disconnect")
        p_socket.disconnect()
    }
    
    func sendIceConfigRequest(callId: String){
        let iceConfig = ["call_id": callId] as [String : Any]
        p_socket.emit("ice_config", iceConfig)
    }
    
    func sendSignalRequest(messageId: String,receiver:String, data: Any){
        
        do {
            let send_message = ["message_id": messageId,
                                "receiver": receiver,
                                "data": data] as [String : Any]
            
            p_socket.emit("signal", send_message)
            
            
        } catch let err {
            print(err)
        }
    }
}


extension String{
    /// Encode a String to Base64
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
    
    /// Decode a String from Base64. Returns nil if unsuccessful.
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self, options: .ignoreUnknownCharacters) else { return nil }
        return String(data: data, encoding: .utf8)
    }
}


extension String {
    
    func parse<D>(to type: D.Type) -> D? where D: Decodable {
        
        let data: Data = self.data(using: .utf8)!
        
        let decoder = JSONDecoder()
        
        do {
            let _object = try decoder.decode(type, from: data)
            return _object
            
        } catch {
            return nil
        }
    }
}

struct SignalObject: Codable {
    var type: String
    var callId: String
}

