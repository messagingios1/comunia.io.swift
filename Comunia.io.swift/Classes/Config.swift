//
//  Config.swift
//  Comunia.io.swift
//
//  Created by Aruna Udayanga on 2021-06-16.
//

import Foundation

public class Config{
    static var p_inst = Config()
//    var s_UrlSrv = "http://34.87.84.246:443"
    var s_UrlSrv = "https://api.comunia.io"
    var baseUrlFile = "http://api.comunia.io:7790"
    var deviceList: [String:String] = [:]
    
    public static func getInst() -> Config{
        return p_inst
    }
    
    public init(){
        if(getUrlSevPreference() != nil){
            s_UrlSrv = getUrlSevPreference()!
        }
    }
    
    public func setUrlSev(sUrl: String){
        s_UrlSrv = sUrl
    }
    
    public func setUrlSevPreference(sUrl: String){
        UserDefaults.standard.set(sUrl, forKey: "url_srv")
    }
    
    public func getUrlSev() -> String{
        return s_UrlSrv
    }
    
    public func getUrlSevPreference() -> String?{
        return UserDefaults.standard.value(forKey: "url_srv") as? String
    }
    
    public func setUser(sUser: String){
        UserDefaults.standard.set(sUser, forKey: "user_name")
    }
    
    public func getUser() -> String?{
        return UserDefaults.standard.value(forKey: "user_name") as? String
    }
    
    public func setApiKey(sVal: String){
        UserDefaults.standard.set(sVal, forKey: "api_key")
    }
    
    public func getApiKey() -> String?{
        return UserDefaults.standard.value(forKey: "api_key") as? String
    }
    
    public class PeerConnection{
        var eState: State?
        var l_ProcessingStartedTime: String?
        var l_LastConnectedTime: String?
        var l_LastDisconnectedTime: String?
    }
}


class WKUserContent {

    static let WEB_VIEW_SEND_SIGNAL: String = "jsMessageSendSignal"
    static let WEB_VIEW_ERROR_HANDLER: String = "jsMessageErrorHandler"
    static let WEB_VIEW_REQUEST_ICE_CONFIG: String = "jsRequestIceConfig"
    static let WEB_VIEW_INCOMING_CALL: String = "jsIncomingCall"
    static let WEB_VIEW_ON_REMOTE_CALL_END: String = "jsOnRemoteCallEnd"
    
}
