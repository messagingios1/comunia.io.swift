//
//  Bundle+Extensions.swift
//  Comunia.io.swift-Resources
//
//  Created by Aruna Udayanga on 2021-06-16.
//

import Foundation

final class BundleResourceClass {
    static let resourceBundle: Bundle = {
        let myBundle = Bundle(for: BundleResourceClass.self)
        
        guard let resourceBundleURL = myBundle.url(
                forResource: "Resources", withExtension: "bundle")
        else {
            fatalError("MySDK.bundle not found!")
        }
        
        guard let resourceBundle = Bundle(url: resourceBundleURL)
        else {
            fatalError("Cannot access MySDK.bundle!")
        }
        return resourceBundle
    }()
}


