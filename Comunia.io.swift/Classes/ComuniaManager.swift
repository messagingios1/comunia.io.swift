//  ComuniaManager.swift
//  Comunia.io.swift
//
//  Created by Aruna Udayanga on 2021-06-16.
//

import Foundation

@objc
public protocol ComuniaConnectionManagerDelegate: class {
    /// Tells the delegate that the current connection success status
    @objc func comuniaConnectionSuccess()
    /// Tells the delegate that the current connection error status
    ///
    /// - Parameters:
    ///   - error: connection error
    @objc func comuniaConnectionError(comuniaError error: String)
}

@objc
public protocol ComuniaIncomingCallDelegate: class {

    @objc func sendComuniaOnSignal(sender: String, data: Any)
}

public class ComuniaManager: ConnectionCallback, CallManagerOnSignalCallbacks {

    /// Delegate called on connection change
    open weak var delegate: ComuniaConnectionManagerDelegate?
    open weak var delegateForCellAction: ComuniaIncomingCallDelegate?
    
    public init(){}
    
    /// Create connection
    public func ComuniaInitConnection(userName: String, apiKey: String){
        ComuniaConnection.getInst().connectionCallback = self
        ComuniaConnection.getInst().connect(user: userName, apiKey: apiKey)
    }
    
    /// connection success
    public func onSuccess() {
        print("Success")
        delegate?.comuniaConnectionSuccess()
    }
    
    /// connection error
    public func onError(sErr: String) {
        print(sErr)
        delegate?.comuniaConnectionError(comuniaError: sErr)
    }
    
    public func initIncomingCallAction(){
        ComuniaConnection.getInst().callManagerOnSignalCallbacks = self
    }
    
    public func onSignalResponse(sender: String, data: Any) {
        delegateForCellAction?.sendComuniaOnSignal(sender: sender, data: data)
    }
    
    public func deinitIncomingCallAction(){
        ComuniaConnection.getInst().callManagerOnSignalCallbacks = nil
    }
}
