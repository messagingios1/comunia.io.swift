//
//  ComuniaWebView.swift
//  Comunia.io.swift
//
//  Created by Aruna Udayanga on 2021-06-15.
//

import UIKit
import WebKit
import AVFoundation
import SwiftyJSON

@objc
public protocol ComuniaVideoViewDelegate: class {
    /// Tells the delegate that the video call request remote user
    @objc func requestVideoCallForRemoteUser()
    /// Tells the delegate that the video call error
    @objc func comuniaVideoViewError(videoViewError error: String)
    /// Tells the delegate that the incoming video call
    @objc func onIncomingCallAction()
    /// Tells the delegate that the video call end
    @objc func onVideoCallEnd()
}

open class ComuniaWebView: UIView, WKScriptMessageHandler, WKNavigationDelegate, CallConfigsCallbacks, CallManagerOnSignalCallbacks  {
    
    fileprivate var webView = WKWebView()
    open weak var delegate: ComuniaVideoViewDelegate?
    
    var webPageName : String = "index"
    var webPageExtension : String = "html"
    
    // values for video call -
    var videoCallId: String = ""
    var isAudio:Bool = true
    var isVideo:Bool = true
    var incomingCallJson:String = ""
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    func initialize(){
        
        ComuniaConnection.getInst().callConfigsCallbacks = self
        ComuniaConnection.getInst().callManagerOnSignalCallbacks = self
        
        //            webView = WKWebView(frame: CGRect(x:0, y:0, width: self.bounds.width, height: self.bounds.height))
        //            webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //            self.addSubview(webView)
        
        let configuration = WKWebViewConfiguration()
        configuration.allowsInlineMediaPlayback = true
        configuration.preferences.javaScriptEnabled = true
        configuration.allowsPictureInPictureMediaPlayback = true
        webView = WKWebView(frame: frame, configuration: configuration)
        
        let contentController = WKUserContentController()
        webView.configuration.userContentController = contentController
        webView.configuration.userContentController.add(self, name: WKUserContent.WEB_VIEW_SEND_SIGNAL)
        webView.configuration.userContentController.add(self, name: WKUserContent.WEB_VIEW_REQUEST_ICE_CONFIG)
        webView.configuration.userContentController.add(self, name: WKUserContent.WEB_VIEW_ERROR_HANDLER)
        webView.configuration.userContentController.add(self, name: WKUserContent.WEB_VIEW_INCOMING_CALL)
        webView.configuration.userContentController.add(self, name: WKUserContent.WEB_VIEW_ON_REMOTE_CALL_END)
        webView.navigationDelegate = self
        webView.scrollView.isScrollEnabled = false
        
        let source = "function captureLog(msg) { window.webkit.messageHandlers.logHandler.postMessage(msg); } window.console.log = captureLog;"
        let script = WKUserScript(source: source, injectionTime: .atDocumentEnd, forMainFrameOnly: false)
        webView.configuration.userContentController.addUserScript(script)
        webView.configuration.userContentController.add(self, name: "logHandler")
        
        let resourceBundle = BundleResourceClass.resourceBundle
        if let url = resourceBundle.url(forResource: "index", withExtension: "html") {
            webView.loadFileURL(url, allowingReadAccessTo: url.deletingLastPathComponent())
        }
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        webView.removeFromSuperview()
        webView.frame = bounds
        addSubview(webView)
    }
    
    // on IceConfigs Response
    public func onIceConfigsResponse(callId: String, configsObject: Any) {
        print("ice config response => CallId:\(callId) json: \(configsObject)")
        
        let onSignalArray = JSON(configsObject)
        let jsonToString = onSignalArray.rawString()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
            self.webView.evaluateJavaScript("onIceConfigs('\(callId)',\(jsonToString ?? ""))", completionHandler: nil)
        }
    }
    
    //  OnSignal Response
    public func onSignalResponse(sender: String, data: Any) {
        print("onSignal response => sender:\(sender) json: \(data)")
        
        let onSignalArray = JSON(data)
        let jsonToString = onSignalArray.rawString()
        
        self.webView.evaluateJavaScript("onSignal('\(sender)',\(jsonToString ?? ""))", completionHandler: nil)
    }
    
    // OnInComingCall -
    public func onInComingCallAction(sender: String, data: Any){
        DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
            self.onSignalResponse(sender: sender, data: data)
        }
    }
    
    // Make video call
    public func makeVideoCall(contact: String) {
        print("remote user => \(contact)")
        self.webView.evaluateJavaScript("makeVideoCall('\(contact)')", completionHandler: nil)
    }
    
    // request IceConfigs
    func requestIceConfigs() {
        print("requesting turn services");
        let identifier = UUID()
        self.videoCallId = identifier.uuidString
        ComuniaConnection.getInst().requestIceConfigs(callId: self.videoCallId)
    }
    
    // Accept Call Request
    public func acceptCallRequest(sender: String){
        print("Accept Call Request response => sender:\(sender) json: \(self.incomingCallJson)")
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            self.webView.evaluateJavaScript("acceptCallRequest('\(sender)',\(self.incomingCallJson))", completionHandler: nil)
        }
    }
    
    // Reject Call
    public func rejectCall(sender: String){
        self.webView.evaluateJavaScript("rejectCall('\(sender)'", completionHandler: nil)
        self.webView.evaluateJavaScript("hangup()", completionHandler: nil)
        ComuniaConnection.getInst().callManagerOnSignalCallbacks = nil
    }
    
    public func isVideoEnable(isVideo:Bool){
        self.webView.evaluateJavaScript("enableVideo(\(isVideo))", completionHandler: nil)
    }
    
    public func isAudioEnable(isAudio:Bool){
        self.webView.evaluateJavaScript("enableAudio(\(isAudio))", completionHandler: nil)
    }
    
    public func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        
        if message.name == WKUserContent.WEB_VIEW_REQUEST_ICE_CONFIG {
            print("Request ice config => \(message.body)")
            requestIceConfigs()
            
        } else if message.name == WKUserContent.WEB_VIEW_ERROR_HANDLER {
            print("Error Messsage => \(message.body)")
            
            let json = JSON(message.body)
            let errorMessage = json["message"].stringValue
            self.delegate?.comuniaVideoViewError(videoViewError: errorMessage)
            
        } else if message.name == WKUserContent.WEB_VIEW_SEND_SIGNAL{
            print("SendSignal => \(message.body)")
            
            let json = JSON(message.body)
            let dataObject = json["message"].rawString()
            let callId = json["message"]["callId"].stringValue
            let remoteUser = json["remoteUser"].stringValue
            
            ComuniaConnection.getInst().sendEventSignal(messageId: callId , receiver: remoteUser, data: dataObject as Any)
            
        } else if message.name == WKUserContent.WEB_VIEW_INCOMING_CALL {
            print("Incoming Call Object => \(message.body)")
            
            let json = JSON(message.body)
            let jsonToString = json.rawString()
            self.incomingCallJson = jsonToString!
            
            self.delegate?.onIncomingCallAction()
            
        }else if message.name == WKUserContent.WEB_VIEW_ON_REMOTE_CALL_END {
            print("On Remote Call End Object => \(message.body)")
            self.delegate?.onVideoCallEnd()
        }
        
        if message.name == "logHandler" {
            print("LOG: \(message.body)")
        }
    }
    
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("End loading")
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            self.delegate?.requestVideoCallForRemoteUser()
        }
    }
}

