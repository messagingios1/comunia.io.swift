//
//  FileService.swift
//  Comunia.io.swift
//
//  Created by Aruna Udayanga on 2021-07-10.
//

import UIKit
import Foundation

struct UploadFileRequest {
    let file_to_upload : Data
}

struct UploadFileResponse :Codable {
    let fileId : String
    let contentType : String
    let status: Int
}

struct FileService{
    var uploadUrl:String = ""
    var downloadUrl:String = ""
    var token:String = ""
    
    init(url:String,token:String) {
        let baseUrlFile = "http://api.comunia.io:7790"
        self.uploadUrl = "\(baseUrlFile)/file/api/v1/file/add"
        self.downloadUrl = "\(baseUrlFile)/file/api/v1/file/get"
        self.token = token
    }
    
    func uploadFiles(data:Data, completionHandler:@escaping(_ result: Any)-> Void){
        let uploadFileRequest = UploadFileRequest(file_to_upload: data)
        postMutipartFormData(requestUrl: URL(string: self.uploadUrl)!, requestBody: uploadFileRequest, token: self.token) { (res) in
            _ = completionHandler(res)
        }
    }
    
    
    func postMutipartFormData(requestUrl: URL, requestBody: UploadFileRequest, token:String, completionHandler:@escaping(_ result: Any)-> Void){
        
        var urlRequest = URLRequest(url: requestUrl)
        urlRequest.httpMethod = "post"
        urlRequest.addValue("Bearer \(token)", forHTTPHeaderField:"Authorization" )
        
        let lineBreak = "\r\n"
        let  boundary = "---------------------------------\(UUID().uuidString)"
        urlRequest.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "content-type")
        
        var data = Data()
        
        data.append("--\(boundary)\r\n" .data(using: .utf8)!)
        data.append("content-disposition: form-data; name=\"file_to_upload\"; filename=\"image1\" \(lineBreak + lineBreak)" .data(using: .utf8)!)
        data.append(requestBody.file_to_upload)
        data.append("\(lineBreak)--\(boundary)--\(lineBreak)" .data(using: .utf8)!)
        
        urlRequest.setValue("\(data.count)", forHTTPHeaderField: "content-length")
        urlRequest.httpBody = data
        
        
        URLSession.shared.dataTask(with: urlRequest) { (data, res, error) in
            
            if let error = error {
                _ = completionHandler(error)
                return
            }
            
            guard let httpResponse = res as? HTTPURLResponse,
                  (200...299).contains(httpResponse.statusCode) else {
                _ = completionHandler(res ?? "Upload failed")
                return
            }
            
            
            do {
                let resString  = String(decoding: data!, as: UTF8.self)
                let jsonData = resString.data(using: .utf8)!
                let uploadFileResponse = try JSONDecoder().decode(UploadFileResponse.self, from: jsonData)
                _ = completionHandler(uploadFileResponse)
            } catch let decodeError {
                debugPrint("TZ \(decodeError)")
                _ = completionHandler(error ?? "Decode failed")
            }
            
            
        }.resume()
    }
    
    func downloadFile(fileId:String,fileType:String ,completionHandler:@escaping(_ result:Any)-> Void){
        
        let url = URL(string: "\(self.downloadUrl)/\(fileId)")!
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "get"
        urlRequest.addValue("Bearer \(token)", forHTTPHeaderField:"Authorization" )
        
        // Create destination URL
        let documentsUrl:URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first)!
        
        let fileName = "\(fileId).\(fileType.split(separator: "/")[1])"
        let destinationFileUrl = documentsUrl.appendingPathComponent("\(fileName)")
        
        let task = URLSession.shared.downloadTask(with: urlRequest) { tempUrl,res,err in
            if let error = err {
                _ = completionHandler(error)
                return
            }
            
            guard let httpResponse = res as? HTTPURLResponse,
                  (200...299).contains(httpResponse.statusCode) else {
                _ = completionHandler(res ?? "Download failed")
                return
            }
            
            if let tempUrl = tempUrl {
                do {
                    try FileManager.default.copyItem(at: tempUrl, to: destinationFileUrl)
                    _ = completionHandler(destinationFileUrl)
                } catch (let writeError) {
                    _ = completionHandler(writeError)
                }
            }else{
                _ = completionHandler(err as Any)
            }
            
        }
        
        task.resume()
    }
}
