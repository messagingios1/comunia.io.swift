//
//  FileService.swift
//  Comunia.io.swift
//
//  Created by Aruna Udayanga on 2021-07-10.
//

import Foundation
import SwiftyJSON

@objc
public protocol ComuniaMessageDelegate: class {

    func receivedOnMesssage(message: String)
    
    func comuniaMessageError(comuniaError error: String, message: Any)
    
    func comuniaFileUploadSuccess(lastUploadedFileName: String, fileId: String, contentType: String, status: Int)
    
    func comuniaFileUploadError(comuniaError error: String)
    
    func comuniaFileDownloadSuccess(message: String)
    
    func comuniaFileDownloadError(comuniaError error: String)
}


public class ComuniaMessage: MessageCallback, FileUploadCallback, FileDownloadCallback  {

    var senderName:String = ""
    var lastUploadedFileName:String = ""
    var lastUploadedFileId:String = ""
    var lastUploadedFileType:String = ""
    
    open weak var delegateMessage: ComuniaMessageDelegate?
    
    public init(){
        ComuniaConnection.getInst().messageCallback = self
        ComuniaConnection.getInst().fileUplocadCallback = self
        ComuniaConnection.getInst().fileDownloadCallback = self
    }

    // Send Chat Message
    public func sendMessage(senderName: String, message: String){
        ComuniaConnection.getInst().sendMessage(to: senderName, message: message)
    }
    
    // Recived Chat Message
    public func onSuccess(message: Message) {
       // let jsonString = JSON(message)
       let dfdfd = message.convertToString ?? ""
        delegateMessage?.receivedOnMesssage(message: dfdfd)
    }
    
    // Chat Mesasge Error Handle
    public func onError(message: Message, error: String) {
        delegateMessage?.comuniaMessageError(comuniaError: error, message: message)
    }
    
    // File Upload
    public func uploadFile(senderName: String, data:[Data], imageUrl:String!){
        
        if let imageName = imageUrl.split(separator: "/").last{
            self.lastUploadedFileName = String(imageName)
        }
        
        self.senderName = senderName
        ComuniaConnection.getInst().uploadFiles(to: senderName, message: self.lastUploadedFileName, files: data)
    }
    
    //File Upload Success
    public func onUploadFileSuccess(message: Any) {
        
        if let info = message as? [UploadFileResponse]{
            
            ComuniaConnection.getInst().sendMessageWithFiles(to: self.senderName, message: self.lastUploadedFileName, fileInfo: info)
            
            delegateMessage?.comuniaFileUploadSuccess(lastUploadedFileName: self.lastUploadedFileName, fileId: info[0].fileId, contentType: info[0].contentType, status: info[0].status)
        }
    }
    
    // File Upload Error Success
    public func onUploadFileErr(message: String) {
        delegateMessage?.comuniaFileUploadError(comuniaError: message)
    }
    
    
    public func fileDownload(fileId:String, fileType:String){
        ComuniaConnection.getInst().downloadFile(fileId: fileId, fileType: fileType)
    }
    
    public func onDownloadFileSuccess(message: Any) {
        delegateMessage?.comuniaFileDownloadSuccess(message: "File downloaded successfully.")
    }
    
    public func onDownloadFileErr(message: String) {
        delegateMessage?.comuniaFileDownloadError(comuniaError: message)
    }
}


