//
//  ComuniaConnection.swift
//  Comunia.io.swift
//
//  Created by Aruna Udayanga on 2021-06-16.
//

import SwiftyJSON

public protocol ConnectionCallback{
    func onSuccess()
    func onError(sErr: String)
}

public protocol MessageCallback{
    func onSuccess(message: Message)
    func onError(message: Message, error: String)
}

public protocol FileUploadCallback{
    func onUploadFileSuccess(message: Any)
    func onUploadFileErr(message: String)
}

public protocol FileDownloadCallback{
    func onDownloadFileSuccess(message: Any)
    func onDownloadFileErr(message: String)
}

public protocol CallManagerCallbacks{
    func onIncomingCall(sender: String, type: String, JSONObject: Any)
}

public protocol CallConfigsCallbacks{
    func onIceConfigsResponse(callId: String, configsObject: Any)
  //  func onSignalResponse(sender: String, data: Any)
}

public protocol CallManagerOnSignalCallbacks{
    func onSignalResponse(sender: String, data: Any)
}

public class ComuniaConnection: Callback{
    
    private static var p_Inst:ComuniaConnection = ComuniaConnection(sUrlSrv: "")
    var p_Skt:SocketIOManager?
    var b_Register:Bool = false
    var connectionCallback: ConnectionCallback? = nil
    var messageCallback: MessageCallback? = nil
    var fileUplocadCallback : FileUploadCallback? = nil
    var fileDownloadCallback : FileDownloadCallback? = nil
    var callManagerCallbacks : CallManagerCallbacks? = nil
    var callConfigsCallbacks : CallConfigsCallbacks? = nil
    var callManagerOnSignalCallbacks : CallManagerOnSignalCallbacks? = nil
    
    public static func getInst() -> ComuniaConnection{
        return p_Inst
    }
    
    public init(sUrlSrv: String){
    }
    
    public func connect(user: String, apiKey: String){
        let sUrl = Config.getInst().getUrlSev()
        let pJDeviceInfo = ["sdk_type":"iOS", "platform_or_browser": "iOS", "version": "1.0", "manufacturer": "apple"]
        b_Register = false
        let pSkt = SocketIOManager(sUrl: sUrl, sUsr: user, sPwd: apiKey, pDeviceInfo: pJDeviceInfo)
        p_Skt = pSkt
        pSkt.p_Callback = self
    }
    
    public func sendMessage(to: String, message: String){
        if(p_Skt != nil){
            let identifier = UUID()
            
            p_Skt?.sendMsg(sId: identifier.uuidString, sTo: to, sMsg: message, type: "new_message")
        }
    }
    
    /// =====================================================
    /// Send Message with Files
    /// =====================================================
    public func sendMessageWithFiles(to: String, message: String, fileInfo:Any){
        guard let skt = p_Skt else {
            return
        }
        
        let identifier = UUID()
        
        skt.p_Callback = self
        skt.sendMsgWithFile(sId: identifier.uuidString, sTo: to, sMsg: message, type: "new_message", iExpireTime: 1000, fileInfo: fileInfo);
    }
    
    
    /// ======================
    /// Upload Files
    /// ======================
    public func uploadFiles(to: String, message:String, files:[Data]){
        guard let skt = p_Skt else {
            return
        }
        
        skt.p_Callback = self
        skt.uploadFiles(to: to, files: files)
    }
    
    /// =====================================================
    /// Download Files
    /// =====================================================
    public func downloadFile(fileId:String, fileType:String){
        guard let skt = p_Skt else {
            return
        }
        
        skt.p_Callback = self
        skt.downloadFile(fileId: fileId, fileType:fileType)
    }

    public func onRegister(pSkt: SocketIOManager, lId: String) {}
    
    public func onRegisterErr(pSkt: SocketIOManager, lId: String, sErr: String) {}
    
    public func onLogin(pSkt: SocketIOManager, sGrp: String, sType: String, sName: String) {
        connectionCallback?.onSuccess()
    }
    
    public func onRelogin(pSkt: SocketIOManager) {}
    
    public func onLoginErr(pSkt: SocketIOManager, sErr: String) {
        connectionCallback?.onError(sErr: sErr)
    }
    
    public func onMessage(message: Message) {
        messageCallback?.onSuccess(message: message)
    }
    
    public func onError(message: Message, error: String){
        messageCallback?.onError(message: message, error: error)
    }
    
    public func onMsg(pSkt: SocketIOManager, lId: UInt64, sFrom: String, sMsg: String) {
    }
    
    public func onSig(pSkt: SocketIOManager, lId: UInt64, sFrom: String, sMsg: String) {
    }
    
    public func onReq(pSkt: SocketIOManager, lId: UInt64, sFrom: String, sReq: String) {
    }
    
    public func onSendReqRet(pSkt: SocketIOManager, lId: UInt64, sFrom: String, sRet: String) {
    }
    
    public func onRes(pSkt: SocketIOManager, lId: UInt64, sFrom: String, sRes: String) {
    }
    
    public func onCmd(pSkt: SocketIOManager, lId: UInt64, sFrom: String, sCmd: String) {
    }
    
    public func onCmdRes(pSkt: SocketIOManager, lId: UInt64, sFrom: String, sCmd: String) {
    }
    
    public func onGet(pSkt: SocketIOManager, lId: UInt64, sFrom: String, sGet: String) {
    }
    
    public func onGetRes(pSkt: SocketIOManager, lId: UInt64, sFrom: String, sRes: String) {
    }
    
    public func onHttp(pSkt: SocketIOManager, lId: UInt64, pData: JSON) {
    }
    
    public func onPingRet(pSkt: SocketIOManager, lId: UInt64, pData: JSON) {
    }
    
    public func onCfgStartRet(pSkt: SocketIOManager, lId: UInt64, sRet: String) {
    }
    
    public func onCfgClsRet(pSkt: SocketIOManager, lId: UInt64, sRet: String) {
    }
    
    public func onFileStart(pSkt: SocketIOManager, lId: UInt64, sFrom: String, iLen: Int, iPktSize: Int, sMsg: String) {
    }
    
    public func onFileProgress(pSkt: SocketIOManager, lId: UInt64, sFrom: String, iProgress: Int, sMsg: String) {
    }
    
    public func onFile(pSkt: SocketIOManager, lId: UInt64, sFrom: String, pFile: FileManager, sMsg: String) {
    }
    
    public func onErr(pSkt: SocketIOManager, sErr: String) {
    }
    
    public func isLowBandwidth(pSkt: SocketIOManager) -> Bool {
        return false
    }
    
    // webrtc Call function
    public func onIncomingCall(sender: String, type: String, JSONObject: Any) {
        callManagerCallbacks?.onIncomingCall(sender: sender, type: type, JSONObject: JSONObject)
    }
    
    // set IceConfigs
    public func onIceConfigsResponse(call_id: String, JSONObject: Any) {
        callConfigsCallbacks?.onIceConfigsResponse(callId: call_id, configsObject: JSONObject)
    }
    
    public func onSignalResponse(sender: String, data : Any) {
        callManagerOnSignalCallbacks?.onSignalResponse(sender: sender, data: data)
    }
    
    public func requestIceConfigs(callId: String){
        if(p_Skt != nil){
            p_Skt?.sendIceConfigRequest(callId: callId)
        }
    }
    
    public func sendEventSignal(messageId: String,receiver:String, data: Any){
        if(p_Skt != nil){
            p_Skt?.sendSignalRequest(messageId:messageId, receiver:receiver, data:data)
        }
    }
    
    public func onUploadFileSuccess(message:Any) {
        fileUplocadCallback?.onUploadFileSuccess(message: message)
    }
    
    public func onUploadFileErr() {
        fileUplocadCallback?.onUploadFileErr(message: "File upload failed")
    }
    
    
    public func onDownloadFileSuccess(message:Any) {
        fileDownloadCallback?.onDownloadFileSuccess(message: message)
    }
    
    public func onDownloadFileErr() {
        fileDownloadCallback?.onDownloadFileErr(message: "File download failed")
    }
}



