//
//  State.swift
//  Comunia.io.swift
//
//  Created by Aruna Udayanga on 2021-06-16.
//

import Foundation

enum State{
    case Init
    case Connected
    case Logged
    
}

public struct Message: Codable {
    var s_Id:String = ""
    var l_Time: Int = 0
    var s_From: String = ""
    var s_To: String = ""
    var s_Data: String = ""
}
