let callManager = new CallManager();

class CallManagerCallbacks {
    sendSignal = (remoteUser, message) => {
        var messgeToPost = {
            remoteUser,
            message
        };
        window.webkit.messageHandlers.jsMessageSendSignal.postMessage(messgeToPost); }
    requestIceConfigs = () => { window.webkit.messageHandlers.jsRequestIceConfig.postMessage('iceConfigs');  }
    onError = (message) => { window.webkit.messageHandlers.jsMessageErrorHandler.postMessage(message); }
}


//requestIceConfigs -> jsAndroidIntefaceCallbacks.requestIceConfigs()
//jsAndroidIntefaceCallbacks.sendSignal(remoteUser, JSON.stringify(message))
//error jsAndroidIntefaceCallbacks.onError(JSON.stringify(message))
callManager.init('local_video', 'remote_video_div', [], new CallManagerCallbacks());

const startLocalStreaming = () => {
    callManager.startLocalStreaming();
}

const makeVideoCall = (remoteUser) => {
    if (!remoteUser)
        throw new Error('please enter a valid remote username')
        
    let callConstrains = {
        audio: true,
        video: true
    };
    
    callManager.sendCallRequest({ remoteUser, constraints: callConstrains });
}

const onIceConfigs = (callId, data) => {
    console.log('onIceConfigs =>' + callId + ' data =' + JSON.stringify(data));
    callManager.onIceConfigs(callId, data)
}

const acceptCallRequest = (remoteUser, options) => {
    if ((typeof options) === 'string')
        options = JSON.parse(options)

    const callOptions = {
        remoteUser: remoteUser,
        callId: options.callId,
        pcConfig: options.pcConfig
    }
    
    console.log('acceptCallRequest +++ =>' + remoteUser + ' data =' + JSON.stringify(callOptions));

    callManager.acceptCallRequest(callOptions)
}

const onSignal = (sender, data) => {
    console.log('on signal Data = ', data);
    try {
        if (typeof data === 'string' || data instanceof String) data = JSON.parse(data)
            
        console.log('on signal sender = ' + sender + ' data = ' + JSON.stringify(data));
        callManager.onSignal(sender, data);
    } catch(error){
        console.log('on signal Error = ', error);
    }
}

const hangup = () => {
    console.log('hangging up the call')
    callManager.hangup();
}

const enableVideo = (enable) => {
    callManager.switchVideo(enable)
}

const enableAudio = (enable) => {
    callManager.switchAudio(enable)
}

const rejectCall = (remoteUser) => {
    callManager.rejectCall(remoteUser)
}

const busy = (remoteUser) => {
    callManager.rejectCall(remoteUser)
}

// event handlers
callManager.addEventListener(callManager.getCallEvents().ON_REMOTE_ANSWERED, (body) => {
    console.log("on remote answered the call body = " + JSON.stringify(body))
    jsAndroidIntefaceCallbacks.onRemoteAnswered(body.remoteUser)
})

callManager.addEventListener(callManager.getCallEvents().ON_REMOTE_CALL_END, (body) => {
    console.log("on remote disconnected = " + JSON.stringify(body))
   // jsAndroidIntefaceCallbacks.onRemoteCallEnd(body.remoteUser)
    window.webkit.messageHandlers.jsOnRemoteCallEnd.postMessage(body.remoteUser);
}) 

callManager.addEventListener(callManager.getCallEvents().ON_MISSED_CALL, (body) => {
    console.log("on missed call = " + JSON.stringify(body))
    jsAndroidIntefaceCallbacks.onMissedCall(body.remoteUser)
})

callManager.addEventListener(callManager.getCallEvents().ON_INCOMING_CALL, (event) => {
   // jsAndroidIntefaceCallbacks.onIncomingCall(event.remoteUser, JSON.stringify(event))
    console.log('ON INCOMING CALL = ', event);
    window.webkit.messageHandlers.jsIncomingCall.postMessage(JSON.stringify(event));
})

callManager.addEventListener(callManager.getCallEvents().ON_REMOTE_USER_BUSY, (body) => {
    console.log('on remote user busy body = ' + JSON.stringify(body));
    jsAndroidIntefaceCallbacks.onRemoteUserBusy(body.remoteUser)
})

callManager.addEventListener(callManager.getCallEvents().ON_REMOTE_USER_REJECTED, (body) => {
    console.log('on remote user rejected body = ' + JSON.stringify(body));
    jsAndroidIntefaceCallbacks.onRemoteUserRejected(body.remoteUser)
})
