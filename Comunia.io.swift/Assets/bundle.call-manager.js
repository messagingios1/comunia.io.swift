var CallManager = (function () {
    'use strict';

    const CALL_EVENTS = {
        ON_INCOMING_CALL: 'onIncomingCall',
        ON_REMOTE_USER_BUSY: 'onRemoteUserBusy',
        ON_REMOTE_USER_REJECTED: 'onRemoteUserRejected',
        ON_REMOTE_ANSWERED: 'onRemoteAnswered',
        ON_REMOTE_CALL_END: 'onRemoteCallEnd',
        ON_MISSED_CALL: 'onMissedCall'
    };

    const VALID_CALL_EVENTS = [
        CALL_EVENTS.ON_INCOMING_CALL, CALL_EVENTS.ON_REMOTE_USER_BUSY, CALL_EVENTS.ON_REMOTE_USER_REJECTED, CALL_EVENTS.ON_REMOTE_ANSWERED, CALL_EVENTS.ON_REMOTE_CALL_END, CALL_EVENTS.ON_MISSED_CALL
    ];

    let _this = null;

    class CallManager {
        constructor() {
            _this = this;
            _this.isInitiator = false;
            _this.isStarted = false;
            _this.localStream = null;
            _this.pc = null;
            _this.remoteStream = null;
            _this.localElementId = null;
            _this.remoteElementId = null;
            _this.listeners = {};
            _this.remoteUser = null; // this variable only set when start the call
            _this.offer = null;
            _this.initialized = false;
            _this.callId = null;

            // initialize the callback with default handlers
            _this.callbacks = {
                onError: (message) => { console.log('onError ' + JSON.stringify(message)); },
                sendSignal: (remoteUser, message) => { console.log('ERROR Please set set signal callback ' + JSON.stringify(message)); },
                requestIceConfigs: () => { console.log('ERROR please enter valid signal callback with requestIceConfigs'); }
            };

            _this.sdpConstraints = {
                'mandatory': {
                    'OfferToReceiveAudio': true,
                    'OfferToReceiveVideo': true
                }
            };

            _this.pcConfig = { iceServers: [] };
        }

        addEventListener(type, callback) {
            if (VALID_CALL_EVENTS.indexOf(type) === -1) throw new Error(`Invalid event type: ${type}`)
            if (!_this.listeners[type]) _this.listeners[type] = [];

            _this.listeners[type].push(callback);
        }

        removeEventListener(type, callback) {
            if (!(type in _this.listeners)) return

            const stack = _this.listeners[type];
            stack.forEach((s, i) => {
                if (s === callback) {
                    stack.splice(i, 1);
                    return
                }
            });
        }

        dispatchEvent(event) {
            if (!(event.type in _this.listeners)) return true

            const stack = _this.listeners[event.type];
            stack.forEach(s => {
                s.call(_this, event.body);
            });
        }

        /**
         * function init
         * @param {String} localVideoId  - id of the local video
         * @param {String} remoteVideoId - id of the remove video or div
         * @param {Object[]} iceServers - an array of turn and stun servers in the following format
         *                                 [{ 'urls': <url>, 'credential': <password>, 'username': <username> }]
         * @param {Object} callbacks - callbacks.sendSignal(message)
         *                             callbacks.onError({type:<error type>, message:<error message>})
         */
        init(localVideoId, remoteVideoId, iceServers, callbacks) {
            console.log('initializing CallManager localVideoid = ' + localVideoId + ' remoteVideoId = ' + remoteVideoId + ' callbacks = ' + callbacks);
            if (callbacks === null || callbacks === undefined)
                throw new Error('please enter a valid callback')
            console.log('initialized CallManager localVideoid = ' + localVideoId + ' remoteVideoId = ' + remoteVideoId + ' callbacks = ' + callbacks);

            _this.localElementId = localVideoId;
            _this.remoteElementId = remoteVideoId;
            _this.callbacks = callbacks;
            _this.loadIceServers(iceServers);
            _this.initialized = true;
        }

        isInitialized() {
            return _this.initialized
        }

        setDefaultRemoteAndLocalVideoElements(localVideoId, remoteVideoId) {
            _this.localElementId = localVideoId;
            _this.remoteElementId = remoteVideoId;
        }

        /**
         * function call
         * Start the call
         */
        async call(callConstrains) {
            _this.isInitiator = true;
            if (!_this.localStream) await _this.startLocalStreaming(callConstrains);
            _this.start();
        }

        /**
         * function onSignal
         * Whenever a signal received, this function should be called and the caller function could be either
         * the mobile app or web app
         *
         * @param {String} sender
         * @param {Object} message
         *
         * the acceptable message formats
         *   {type:'call_request'}
         *   {type:'call_accepted'}
         *   {type:'offer', sdp:<session description>}
         *   {type:'candidate', candidate:<ICE candidate>, id:<event.candidate.sdpMid>, label:<event.candidate.sdpMLineIndex>}
         *   {type:'candidate', candidate:<ICE candidate>, id:<event.candidate.sdpMid>, label:<event.candidate.sdpMLineIndex>}
         *   {type:'candidate', candidate:<ICE candidate>, id:<event.candidate.sdpMid>, label:<event.candidate.sdpMLineIndex>}
         *   {type:'answer', sdp:<session description>}
         *   {type:'disconnected'}
         */
        async onSignal(sender, message) {
            if (!message)
                return this.onError('ProcessingReceivedSignal', 'please enter a non-empty and not null message')

            if ((typeof message) === 'string')
                message = JSON.parse(message);

            if (!message.type)
                return this.onError('ProcessingReceivedSignal', 'please enter a valid json message with "type"')

            if (message.type !== 'call_request') {
                if (!_this.remoteUser || _this.remoteUser !== sender)
                    return _this.onError('ProcessingReceivedSignal', 'signal received from other users [' + JSON.stringify(message) + ']')
            }

            switch (message.type) {
                case 'call_request': return _this.onIncommingCal(sender, message)
                case 'call_accepted': return _this.onCallAccepted()
                case 'call_error': return _this.onCallError(message)
                case 'offer': return _this.onOfferSignal(sender, message)
                case 'answer': return _this.onAnswerSignal(message)
                case 'candidate': return _this.onCandidateSignal(message)
                case 'disconnected': return _this.onDisconnectedSignal()
                case 'busy': return _this.dispatchEvent({ type: CALL_EVENTS.ON_REMOTE_USER_BUSY, body: { remoteUser: _this.remoteUser } })
                case 'call_rejected': return _this.dispatchEvent({ type: CALL_EVENTS.ON_REMOTE_USER_REJECTED, body: { remoteUser: _this.remoteUser } })
                default: return _this.onError('processingSignals', 'unhandled signal type [' + JSON.stringify(message) + ']')
            }
        }

        onIncommingCal(sender, message) {
            if (_this.remoteUser)
                _this.busy(sender);

            _this.dispatchEvent({
                type: CALL_EVENTS.ON_INCOMING_CALL,
                body: { remoteUser: sender, constraints: message.constraints, callId: message.callId, pcConfig: message.pcConfig }
            });
        }

        onIceConfigs(callId, configs) {
            if (typeof configs === 'string' || configs instanceof String) configs = JSON.parse(configs)
                
            console.log('onIceConfigs_call_configs =>' + JSON.stringify(configs));
            try{
            _this.callId = callId;

            // the format of configs
            // {turnDetails:{host:<host of the turn server>,credentials:{userName:<username>,password:<password>}},stunDetails:[<stun server1>, <stun server2>]}
            let servers = [];
            servers.push({ 'urls': configs.turnDetails.host, 'username': configs.turnDetails.credentials.userName, 'credential': configs.turnDetails.credentials.password });
            let length = configs.stunDetails.length;
            for (let i = 0; i < length; i++)
                servers.push({ 'urls': configs.stunDetails[i] });
            _this.loadIceServers(servers);
            _this.startSignalling();
                
            }catch(error){
                console.log('onIceConfigs_call_error =>' + error);
            }
        }

        /**
         * function onCallAccepted
         * this function is called when the remote user has accepted the call request
         */
        onCallAccepted() {
            _this.call();
        }

        /**
         * function onCallError
         * this function is called when there is a error while initializing the call in the remote user
         *   error while getting local streaming of the remote user
         *   user denied to grant permissions to access the camera or microphone in the remote user
         * @param {Object} message
         */
        onCallError(message) {
            _this.onError('remoteCall', message);
        }

        onOfferSignal(sender, message) {
            if (_this.offer) {
                _this.dispatchEvent({ type: CALL_EVENTS.ON_MISSED_CALL, body: { remoteUser: sender } });
                return;
            }

            _this.remoteUser = sender;
            _this.offer = message;
            _this.answerCall();
        }

        onAnswerSignal(message) {
            if (!_this.isStarted)
                return _this.onError('SignalProcessing', 'received answer before starting the call')

            _this.pc.setRemoteDescription(new RTCSessionDescription(message));
            _this.dispatchEvent({ type: CALL_EVENTS.ON_REMOTE_ANSWERED, body: { remoteUser: _this.remoteUser } });
        }

        onCandidateSignal(message) {
            if (!_this.isStarted)
                return _this.onError('SignalProcessing', 'received candidate before starting the call')

            let candidate = new RTCIceCandidate({
                sdpMLineIndex: message.label,
                candidate: message.candidate
            });
            _this.pc.addIceCandidate(candidate);
        }

        onDisconnectedSignal() {
            console.log('on disconnected signal');
            if (!_this.isStarted)
                return _this.onError('SignalProcessing', 'received disconnected signal before starting the call')

            _this.handleRemoteHangup();
        }

        switchAudio(turnOn) {
            _this.localStream.getAudioTracks().forEach(t => t.enabled = turnOn);
        }

        switchVideo(turnOn) {
            _this.localStream.getVideoTracks().forEach(t => t.enabled = turnOn);
        }

        /**
         * function startLocalStreaming
         * @param {Object} callConstrains - {audo:<boolean>, video:<boolean>}
         *                                  the default value of this parameter is {audio:true, video:true}
         */
        startLocalStreaming(callConstrains) {
            return new Promise((resolve, reject) => {
                if (callConstrains == undefined || callConstrains == null) {
                    callConstrains = {
                        audio: true,
                        video: true
                    };
                }

                console.log('local video id = ' + _this.localElementId + " remote video id = " + _this.remoteElementId);

                let localVideo = document.getElementById(_this.localElementId);

                const gotStream = (stream) => {
                    _this.localStream = stream;
                    localVideo.srcObject = stream;
                    _this.startSignalling();
                    resolve();
                };

                navigator.mediaDevices.getUserMedia(callConstrains).then(gotStream).catch((e) => {
                    _this.onError('getUserMedia', 'unable to get user media [' + e.message + ']');
                    if (!_this.isInitiator)
                        _this.sendSignal({ type: 'call_error', state: 'error', message: 'unable to get local stream error-name=' + e.name + ' message = [' + e.message + ']' });
                });
            })
        }

        startSignalling() {
            if (!_this.localStream || !_this.callId) return;

            if (_this.isInitiator)
                _this.sendSignal({ type: 'call_request', pcConfig: _this.pcConfig });
            else
                _this.sendSignal({ type: 'call_accepted' });
        }

        /**
         * function sendSignal
         * if this function is called without remoteUser _this.remoteUser is used
         * @param {Object} message
         * @param {String} remoteUser optional
         */
        sendSignal(message, remoteUser) {
            if (!remoteUser)
                remoteUser = _this.remoteUser;

            message.callId = _this.callId;
            _this.callbacks.sendSignal(remoteUser, message);
        }

        onError(type, message) {
            console.log('on error type = ' + type + ' message = ' + message);
            _this.callbacks.onError({ type: type, message: message });
        }

        /**
         * function start
         * Create a peer-connection if it haven't been started
         * This function is called when calling or received an offer
         */
        start() {
            if (!_this.isStarted && typeof _this.localStream !== 'undefined' && _this.callbacks) {
                _this.createPeerConnection();
                _this.pc.addStream(_this.localStream);
                _this.isStarted = true;
                if (_this.isInitiator) {
                    _this.doCall();
                }
            }
        }

        createPeerConnection() {
            try {
                _this.pc = new RTCPeerConnection(_this.pcConfig);
                _this.pc.onicecandidate = _this.handleIceCandidate;
                _this.pc.onaddstream = _this.handleRemoteStreamAdded;
                _this.pc.onremovestream = _this.handleRemoteStreamRemoved;
            } catch (e) {
                _this.onError('createPeerConnection', e.message);
                return
            }
        }

        /**
         * function handleIceCandidate
         * this function is called by ICE framework
         * if this function is called without event.candidate, that indicated the end of candiates
         */
        handleIceCandidate(event) {
            if (!event.candidate)
                return

            _this.sendSignal({
                type: 'candidate',
                label: event.candidate.sdpMLineIndex,
                id: event.candidate.sdpMid,
                candidate: event.candidate.candidate
            });
        }

        handleCreateOfferError(event) {
            console.log('createOffer() error: ', event);
            _this.onError('createOffer', 'unable to create offer [' + JSON.stringify(event) + ']');
        }

        doCall() {
            console.log('sdp constraints = ' + JSON.stringify(_this.sdpConstraints));
            _this.pc.createOffer(_this.setLocalAndSendMessage, _this.handleCreateOfferError, _this.sdpConstraints);
        }

        async answerCall() {
            if (!_this.localStream) await _this.startLocalStreaming();
            if (!_this.isInitiator && !_this.isStarted) _this.start();
            _this.pc.setRemoteDescription(new RTCSessionDescription(_this.offer));

            /*_this.pc.createAnswer().then(
                _this.setLocalAndSendMessage,
                _this.onCreateSessionDescriptionError
            )*/
            console.log('sdp constraints = ' + JSON.stringify(_this.sdpConstraints));
            _this.pc.createAnswer(
                _this.setLocalAndSendMessage,
                _this.onCreateSessionDescriptionError,
                _this.sdpConstraints
            );
        }

        rejectCall(remoteUser) {
            _this.sendSignal({ type: 'call_rejected' }, remoteUser);
        }

        busy(remoteUser) {
            _this.sendSignal({ type: 'busy' }, remoteUser);
        }

        sendCallRequest({ localVideoElementId, remoteVideoElementId, remoteUser, constraints }) {
            _this.isInitiator = true;
            _this.remoteUser = remoteUser;
            if (localVideoElementId) _this.localElementId = localVideoElementId;
            if (remoteVideoElementId) _this.remoteElementId = remoteVideoElementId;
            _this.startLocalStreaming(constraints);
            _this.callbacks.requestIceConfigs();
        }

        acceptCallRequest({ callId, pcConfig, remoteUser, constraints, remoteVideoElementId, localVideoElementId }) {
            _this.callId = callId;
            _this.pcConfig = pcConfig;
            _this.remoteUser = remoteUser;
            _this.startLocalStreaming(constraints);

            if (localVideoElementId)
                _this.localElementId = localVideoElementId;

            if (remoteVideoElementId)
                _this.remoteElementId = remoteVideoElementId;
        }

        /**
         * function setLocalAndSendMessage
         * Set the offer or the answer as the local description and send it to the peer
         * This function is called when creating answer and offer
         * @param {Object} sessionDescription
         */
        setLocalAndSendMessage(sessionDescription) {
            _this.pc.setLocalDescription(sessionDescription);
            _this.sendSignal(sessionDescription);
        }

        onCreateSessionDescriptionError(error) {
            _this.onError('CreateSessionDescription', error.toString());
        }

        loadIceServers(servers) {
            let len = servers.length;
            for (let i = 0; i < len; i++) {
                _this.pcConfig.iceServers.push(servers[i]);
            }
        }

        /**
         * function handleRemoteStreamAdded
         * this function is called when the remote stream added
         *
         * if the given remote element id is a 'video', set the stream directly
         * if the given remote element id is not a 'video', create a video element and
         *   append that as a child of the remote video element. This is essenal in order
         *   to make this work in the Android WebView
         * @param {Object} event
         */
        handleRemoteStreamAdded(event) {
            const remoteVideoElement = document.getElementById(_this.remoteElementId);
            _this.remoteStream = event.stream;
            let videoElement = null;

            if (remoteVideoElement.nodeName.toLowerCase() === 'video') {
                videoElement = remoteVideoElement;
            }
            else {
                videoElement = document.createElement('video');
                videoElement.srcObject = _this.remoteStream;
                videoElement.autoplay = true;
                videoElement.playsInline = true;
                videoElement.muted = false;
                videoElement.width = remoteVideoElement.offsetWidth;
                videoElement.height = remoteVideoElement.offsetHeight;
                remoteVideoElement.appendChild(videoElement);
            }

            videoElement.srcObject = _this.remoteStream;
        }

        /**
         * function handleRemoteStreamRemoved
         * @param {Object} event
         */
        handleRemoteStreamRemoved(event) {
            console.log('Remote stream removed. Event: ', event);
        }

        /**
         * function hangup
         * send the disconnected message to the peer and stop
         */
        hangup() {
            _this.stop();
            _this.sendSignal({ type: 'disconnected' });
        }

        /**
         * function handleRemoteHangup
         * handle the remove hangup
         */
        handleRemoteHangup() {
            console.log('handleRemoteHangup');
            _this.stop();
            _this.isInitiator = false;
            _this.remoteUser = null;
        }

        /**
         * function stop
         * close the peer connection
         */
        stop() {
            console.log('sending disconnected event');
            _this.dispatchEvent({ type: CALL_EVENTS.ON_REMOTE_CALL_END, body: { remoteUser: _this.remoteUser } });

            try {
                _this.isStarted = false;

                if (_this.pc)
                    _this.pc.close();

                if (_this.localStream)
                    _this.localStream.getTracks().forEach(track => track.stop());

                _this.pc = null;
                _this.offer = null;
                _this.isInitiator = false;
                _this.localStream = null;
                _this.remoteStream = null;
                _this.callId = null;
                _this.pcConfig = { iceServers: [] };
            }
            catch (e) {
                console.log('exception = ' + e.message);
            }
        }

        /**
         * function getCallEvents
         *
         * get a map of call events
         * this function could assist develipers in registering event listeners
         */
        getCallEvents() {
            return CALL_EVENTS
        }
    }

    return CallManager;

}());
