#
# Be sure to run `pod lib lint Comunia.io.swift.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Comunia.io.swift'
  s.version          = '1.0.1'
  s.summary          = 'Connecting to Comunia platform'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
"You need to obtain a API key from comunia platform to start using this package. So first go to Comunia Website and register for account and create a new App where you can find your API Key."
                       DESC

  s.homepage         = 'https://bitbucket.org/messagingios1/comunia.io.swift.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Comunia Teams' => 'comunia@gmail.com' }
  s.source           = { :git => 'https://bitbucket.org/messagingios1/comunia.io.swift.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'
  
  s.swift_version = '5.0'

  s.source_files = 'Comunia.io.swift/Classes/**/*'
  
  s.resource_bundles = {
     'Resources' => ['Comunia.io.swift/Assets/*']
   }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'Socket.IO-Client-Swift', '~> 15.2.0'
  s.dependency 'SwiftyJSON'
end
